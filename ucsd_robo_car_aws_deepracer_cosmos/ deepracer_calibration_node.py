import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
from deepracer_interfaces_pkg.msg import ServoCtrlMsg
from rclpy.parameter import Parameter
from cv_bridge import CvBridge
import cv2
import numpy as np
import os.path

# Nodes in this program
DRC_NODE_NAME = 'deepracer_calibration_node'

# Nodes listening to rosparameters
LG_NODE_NAME = 'lane_guidance_node'
LD_NODE_NAME = 'lane_detection_node'

# Topics subscribed/published to in this program
CAMERA_TOPIC_NAME = '/camera_pkg/display_mjpeg'
THROTTLE_AND_STEERING_TOPIC_NAME = '/ctrl_pkg/servo_msg'

cv2.namedWindow('sliders')


def callback(x):
    pass


def slider_to_normalized(slider_input):
    input_start = 0
    input_end = 2000
    output_start = -1
    output_end = 1
    normalized_output = output_start + (slider_input - input_start) * (
            (output_end - output_start) / (input_end - input_start))
    return normalized_output


lowH = 5
highH = 130
lowS = 0
highS = 130
lowV = 3
highV = 160


steer_left = 0
steer_straight = 1000
steer_right = 2000

steer_sensitivity_max = 100
steer_sensitivity_default = 100

throttle_reverse = 0
throttle_neutral = 1100
throttle_forward = 2000

zero_error_throttle_mode = 0
error_throttle_mode = 1

cv2.createTrackbar('lowH', 'sliders', lowH, highH, callback)
cv2.createTrackbar('highH', 'sliders', highH, highH, callback)
cv2.createTrackbar('lowS', 'sliders', lowS, highS, callback)
cv2.createTrackbar('highS', 'sliders', highS, highS, callback)
cv2.createTrackbar('lowV', 'sliders', lowV, highV, callback)
cv2.createTrackbar('highV', 'sliders', highV, highV, callback)


cv2.createTrackbar('Steering_sensitivity', 'sliders', steer_sensitivity_default, steer_sensitivity_max, callback)
cv2.createTrackbar('Steering_value', 'sliders', steer_straight, steer_right, callback)
cv2.createTrackbar('Throttle_mode', 'sliders', zero_error_throttle_mode, error_throttle_mode, callback)
cv2.createTrackbar('Throttle_value', 'sliders', throttle_neutral, throttle_forward, callback)


class DeepRacerCalibration(Node):
    def __init__(self):
        super().__init__(DRC_NODE_NAME)
        self.throttle_and_steering_publisher = self.create_publisher(ServoCtrlMsg, THROTTLE_AND_STEERING_TOPIC_NAME, 10)
        self.throttle_and_steering_publisher
        self.throttle_and_steering_values = ServoCtrlMsg()
        self.camera_subscriber = self.create_subscription(Image, CAMERA_TOPIC_NAME, self.live_calibration_values, 10)
        self.camera_subscriber
        self.bridge = CvBridge()

        # declare parameters
        self.declare_parameter('Hue_low')
        self.declare_parameter('Hue_high')
        self.declare_parameter('Saturation_low')
        self.declare_parameter('Saturation_high')
        self.declare_parameter('Value_low')
        self.declare_parameter('Value_high')

        self.declare_parameter('Steering_sensitivity')
        self.declare_parameter('zero_error_throttle')
        self.declare_parameter('error_throttle')

        # setting default values for actuators
        self.zero_error_throttle = 0.55
        self.error_throttle = 0.5

        self.max_num_lines_detected = 10
        self.error_threshold = 0.1


    def live_calibration_values(self, data):
        # get trackbar positions
        lowH = cv2.getTrackbarPos('lowH', 'sliders')
        highH = cv2.getTrackbarPos('highH', 'sliders')
        lowS = cv2.getTrackbarPos('lowS', 'sliders')
        highS = cv2.getTrackbarPos('highS', 'sliders')
        lowV = cv2.getTrackbarPos('lowV', 'sliders')
        highV = cv2.getTrackbarPos('highV', 'sliders')

        steer_input = cv2.getTrackbarPos('Steering_value', 'sliders')
        Steering_sensitivity = float(cv2.getTrackbarPos('Steering_sensitivity', 'sliders')/100)
        Throttle_mode = cv2.getTrackbarPos('Throttle_mode', 'sliders')
        throttle_input = cv2.getTrackbarPos('Throttle_value', 'sliders')


        if Throttle_mode == 0:
            self.zero_error_throttle = slider_to_normalized(throttle_input)
        else:
            self.error_throttle = slider_to_normalized(throttle_input)

        # Publish throttle and steering values
        self.throttle_and_steering_values.angle = Steering_sensitivity*slider_to_normalized(steer_input)
        self.throttle_and_steering_values.throttle = slider_to_normalized(throttle_input)
        self.throttle_and_steering_publisher.publish(self.throttle_and_steering_values)

        # Image processing from slider values
        image = self.bridge.imgmsg_to_cv2(data)
        hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        lower_brown = np.array([lowH, lowS, lowV])
        upper_brown = np.array([highH, highS, highV])
        mask = cv2.inRange(hsv, lower_brown, upper_brown)  

        res = cv2.bitwise_and(image, hsv, mask=mask)
        blur = cv2.GaussianBlur(mask, (17, 17), 0)
        frame  = cv2.Canny(blur, 150, 300)

        # plotting results
        cv2.imshow('frame', frame)
        cv2.waitKey(1)

        # Write parameters to yaml file for storage
        config_path = '/home/deepracer/projects/deepracer_ws/src/ucsd_robo_car_aws_deepracer_cosmos/config/deepracer_config.yaml'
        f = open(config_path, "w")
        f.write(
            f"{LD_NODE_NAME}: \n"
            f"  ros__parameters: \n"
            f"    Hue_low : {lowH} \n"
            f"    Hue_high : {highH} \n"
            f"    Saturation_low : {lowS} \n"
            f"    Saturation_high : {highS} \n"
            f"    Value_low : {lowV} \n"
            f"    Value_high : {highV} \n"

            f"{LG_NODE_NAME}: \n"
            f"  ros__parameters: \n"
            f"    steering_sensitivity : {Steering_sensitivity} \n"
            f"    no_error_throttle : {self.zero_error_throttle} \n"
            f"    error_throttle : {self.error_throttle} \n"
        )
        f.close()


def main(args=None):
    rclpy.init(args=args)
    DRC_publisher = DeepRacerCalibration()
    try:
        rclpy.spin(DRC_publisher)
        DRC_publisher.destroy_node()
        rclpy.shutdown()
    except KeyboardInterrupt:
        print(f"\nShutting down {DRC_NODE_NAME}...")
        DRC_publisher.throttle_and_steering_values.angle = 0.0
        DRC_publisher.throttle_and_steering_values.throttle = 0.0
        DRC_publisher.throttle_and_steering_publisher.publish(DRC_publisher.throttle_and_steering_values)
        cv2.destroyAllWindows()
        DRC_publisher.destroy_node()
        rclpy.shutdown()
        print(f"{DRC_NODE_NAME} shut down successfully.")


if __name__ == '__main__':
    main()
